//function

function sayMyName(name){
    console.log('My name is ' + name)
}

// 

sayMyName('Slim Shady')

// You can re-use functions by invoking them at anytime or any part of your code.
// make sure you have declared them first.
sayMyName('poi');
sayMyName('bebs');

// you can also pass variables as argument to a function
let myName = 'chen2'
sayMyName(myName);
// 

// You can use arg and parameter to make the data inside your function dynamic.
// Dynamic - changeable
function sumOfTwoNumbers(firstNum, secondNum){
    let sum = 0
    sum = firstNum + secondNum

    console.log(sum);
};
sumOfTwoNumbers(10, 15);
sumOfTwoNumbers(2, 3);
sumOfTwoNumbers(100,500);
// 

// you can pass a function as an argument
function argumentFunction(){
    console.log('This is a function that was passed as an argument')
};

function parameterFunction(animal){
    argumentFunction();
};
parameterFunction(argumentFunction);
// 



// REAL WORLD APPLICATION

/*  
    Imagine a product page where you have an "add to cart" button where in you have the button
    element itself displayed on the page BUT you don't have the functionality of it yet.
    this is where passing a function as an arg comes in, in order for you to add or have access to a
    function to add functionality to your button once it is clicked.
*/
function addProductToCart(){
    console.log('Click me to add product to cart');
}

let product1 = document.querySelector('#product1');

product1.addEventListener('click', addProductToCart);
// 


// more or less in parameter and arg, javascript wont throw and error, instead it will ignore the extra and turn the lacking parameter into undefined.
function displayFullName(firstName, favFood, pet){
    console.log('About Me:' + firstName);
    console.log('My Fav food:' + favFood);
    console.log('My Pet are:' + pet);
}

displayFullName('Chen');
displayFullName('Miso');
displayFullName('Tash, Cholo, Alley, Q');
// 

// STRING INTERPOLATION - instead of concatination, PAGPASOK OR PAGINJECT NG VARIABLE
function displayMovieDetails(title, synopsis, director){
    console.log(`The Movie is titled: ${title}`);
    console.log(`The Synopsis is: ${synopsis}`);
    console.log(`The Director is: ${director}`);
}
displayMovieDetails('Sisterakas', 'djfksdfnksjdf', 'ako');
// 


// 
function printTwoNames (userName1, userName2){
    let twoNames = `First Name is ${userName1}, Second Name is ${userName2}`
    return twoNames
}
console.log(printTwoNames('Chen', 'Poy'));
// console.log(`Ohayo! I am ${myNameIs()}`);
// 

// NOTE: use double quote for string to display apostrophes in console

// PARAMETER KUNG ANO ANG IPAPASA
// ARGUMENT KUNG ANU YUNG IPAPASA

function displayPlayerDetails(name, age, playerClass){
    // console.log(`Player name: ${name}`);
    // console.log(`Player name: ${age}`);
    // console.log(`Player name: ${playerClass}`);
    let playerDetails = `Name: ${name}, Age: ${age}, Class: ${playerClass}`

    return playerDetails
}
console.log(displayPlayerDetails('Kobi', 120, 'Paladin'));

